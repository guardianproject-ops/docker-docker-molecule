FROM quay.io/ansible/molecule:3.0.8
#FROM registry.gitlab.com/guardianproject-ops/docker-docker-molecule/molecule:3.0.2

ARG ANSIBLE_VERSION
ARG TESTINFRA_VERSION===5.2.2
ARG ANSIBLE_LINT_VERSION=>=4.3.5,<5.0

RUN pip3 install --upgrade pip setuptools && \
    pip3 uninstall -y ansible && pip3 install "ansible${ANSIBLE_VERSION}" "testinfra${TESTINFRA_VERSION}" boto3 boto && \
    pip3 uninstall -y ansible-lint && pip3 install "ansible-lint${ANSIBLE_LINT_VERSION}" && \
    apk add --update --no-cache git make
ADD ./test /test
COPY dockerd-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
