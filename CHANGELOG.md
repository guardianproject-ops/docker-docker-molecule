# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://gitlab.com/guardianproject-ops/docker-docker-molecule/compare/v2.0.0...v2.1.0) (2020-05-12)


### Features

* Add boto module ([9f7b519](https://gitlab.com/guardianproject-ops/docker-docker-molecule/commit/9f7b519d09b3dffa6eb302f34211b227e930a90b))

## [2.0.0](https://gitlab.com/guardianproject-ops/docker-docker-molecule/compare/v1.0.0...v2.0.0) (2020-05-12)


### ⚠ BREAKING CHANGES

* molecule 3.0 has breaking changes.

### Features

* Add boto3 to image ([03ec122](https://gitlab.com/guardianproject-ops/docker-docker-molecule/commit/03ec122c6315940cc60ff602de5e93a6271bf12f))
* Update molecule to 3.0 ([94dc620](https://gitlab.com/guardianproject-ops/docker-docker-molecule/commit/94dc62085760948c40ba95ea4cb349d89e571ee4))


### Bug Fixes

* Use custom molecule image for now. ([37f37bd](https://gitlab.com/guardianproject-ops/docker-docker-molecule/commit/37f37bd24a1ec98c10ac331addc7dda9c0e49e3d))
* Use python3 in test, as python2 no longer exists in the base image ([06243b7](https://gitlab.com/guardianproject-ops/docker-docker-molecule/commit/06243b7bd26cc9a482bae5fc095d0dbc42a4ba6a))

## 1.0.0 (2020-03-27)
